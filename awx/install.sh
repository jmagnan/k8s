#!/bin/bash

# Variables
NAMESPACE=awx
AWX_HOST="awx.magnan.local"
RELEASE_TAG=`curl -s https://api.github.com/repos/ansible/awx-operator/releases/latest | grep tag_name | cut -d '"' -f 4`

# Récupération du dépots
git clone https://github.com/ansible/awx-operator.git
cd awx-operator
git checkout $RELEASE_TAG

# Déploiement awx-operator
make deploy
cd ..

# Génération des certificats
openssl req -x509 -nodes -days 3650 -newkey rsa:2048 -out ./base/tls.crt -keyout ./base/tls.key -subj "/CN=${AWX_HOST}/O=${AWX_HOST}" -addext "subjectAltName = DNS:${AWX_HOST}"

kubectl apply -k base
