#!/bin/bash

# Ajout de la configuration de MetalLB
kubectl apply -f metallb-config.yml

# Génération du secret
kubectl create secret generic -n metallb-system metallb-memberlist --from-literal=secretkey="$(openssl rand -base64 128)"
